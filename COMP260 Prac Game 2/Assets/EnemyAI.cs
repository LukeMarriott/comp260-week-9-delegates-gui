﻿using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour {

	public GameObject Puck;
	public GameObject redGoal; 
	public float moveSpeed;
	private Vector3 puckToRed;
	private Vector3 aiTravel;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		//puckToRed = redGoal.transform.position - Puck.transform.position;
		//aiTravel = transform.position + puckToRed;

		aiTravel = transform.position - Puck.transform.position;
		float mag = aiTravel.magnitude;
		bool hasHitPuck = false;

		print(mag);
		if (mag > 1 && !hasHitPuck) {
			transform.position = Vector3.MoveTowards (transform.position, Puck.transform.position, moveSpeed);
			hasHitPuck = true;
		} else if (mag <=1 && hasHitPuck) {
			transform.position = Vector3.MoveTowards (transform.position, redGoal.transform.position, moveSpeed);
		} else {
			if (mag >= 2) {
				hasHitPuck = false;
			}
		}



		//transform.position = Vector3.MoveTowards (transform.position, Puck.transform.position+aiTravel, moveSpeed);



	}
}
